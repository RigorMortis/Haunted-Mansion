Projekti on työstetty Unityn avulla. Pohjana on käytetty John Lemonin tutorialia https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner
Kaikki projektissa käytetyt grafiikat on John Lemonin tuottamat. Itse olen muokannut hahmojen toimintaa, pelin tarkoitusta, alkua ja loppua. Sijoitellut hahmoja ympäri taloa, rakentanut hahmoille "hitbox":it jotta pelaaja voi törmätä niihin ja hävitä pelin.
Projekti on suoritettu itsenäisenä harraste projektina sillä tykkään todella paljon tietokone peleistä ja olen itsenäisesti opiskellut Unity:a sekä sen mahdollisuuksia.
Peliä pääsee pelaamalla lataamalla lähdekoodin zip tiedostona, purkamalla sen ja käynistämällä ReadyGame kansiossa olevan Haunted Mansion.exe tiedoston.
Pelin tarkoitus on päästä läbyrintin loppuun väistämällä haamut ja patsaat.
